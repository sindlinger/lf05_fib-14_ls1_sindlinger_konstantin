﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static void main(String[] args) {
		boolean go = true;
		
		while(go != false) {
		Scanner tastatur = new Scanner(System.in);
		
		//boolean go = true;
		
		//while(go) {
		double zuZahlenderBetrag; 
		double eingezahlterGesamtbetrag;
       
	   	
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rückgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}	    
	}

	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		
		double zuZahlenderBetrag = 0;
		
		  System.out.println("\nFahrkartenbestellvorgang:");
		  for (int i = 0; i < 25; i++)
		  {
		    System.out.print("=");
		     try {
			    Thread.sleep(150);
			    } catch (InterruptedException e) {
				 // TODO Auto-generated catch block
					e.printStackTrace();
				 }
		       }
		       System.out.println("\n\n");	
		       
		       
		System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		
		int ticketwahl = 999999;
		while(ticketwahl > 3) {
			System.out.println("\n\nIhre Wahl:");
			ticketwahl = tastatur.nextInt();
			
			if(ticketwahl == 1) {
				zuZahlenderBetrag = 2.90;
			}
			else if (ticketwahl == 2) {
				zuZahlenderBetrag = 8.60;
			}
			else if (ticketwahl == 3) {
				zuZahlenderBetrag = 23.50;
			}
			else {
				System.out.println(">>Falsche Eingabe<<");
			}
		}
       	
		
		
		System.out.println("Bitte geben Sie die Anzahl der Tickets ein:");
       	byte x=1;
       	byte tickets = tastatur.nextByte();
   		
   		if (0<tickets &&  tickets<=10) {
   			x= tickets;
   		}
   		else {
   			System.out.println("falsch");
   			//return 1; 			
   		}

   		return zuZahlenderBetrag * x;
		
		}
   
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.00;
	    
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	    {
	    System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    System.out.print("Eingabe (mind. 5Ct, höchstens 2.00 Euro): ");
	    double eingeworfeneMünze = tastatur.nextDouble();
	    eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze;
	    }

		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
	  System.out.println("\nFahrschein wird ausgegeben");
	  for (int i = 0; i < 8; i++)
	  {
	    System.out.print("=");
	     try {
		    Thread.sleep(250);
		    } catch (InterruptedException e) {
			 // TODO Auto-generated catch block
				e.printStackTrace();
			 }
	       }
	       System.out.println("\n\n");	
	}
	
	public static void rückgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       
		if(rückgabebetrag > 0.0) {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }  	 
}
