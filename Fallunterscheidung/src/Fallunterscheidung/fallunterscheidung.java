package Fallunterscheidung;
import java.util.Scanner;



public class fallunterscheidung {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		note();
		monate();
		roemischeZahlen();
		taschenrechner(); 
	}

//Aufgabe 1 - Noten
	public static void note() {
		Scanner tastatur = new Scanner(System.in);  
		System.out.println("Geben Sie eine Note zwischen 1 und 6 ein:");
	    int note = tastatur.nextInt();
	    
	    if(note == 1) {
	    	System.out.println("Die Note ist sehr gut."); }
	    else if(note == 2) {
	    	System.out.println("Die Note ist gut."); }
	    else if(note == 3) {
		   	System.out.println("Die Note ist befriedigend."); }
		else if(note == 4) {
		   	System.out.println("Die Note ist ausreichend."); }
	   	else if(note == 5) {
	   		System.out.println("Die Note ist mangelhaft."); }
	   	else if(note == 6) {
	    	System.out.println("Die Note ist ungen�gend."); }
	   	else {
	   		System.out.println("Sie m�ssen eine Zahl zwischen 1 und 6 eingeben."); }   
	}

//Aufgabe 2	- Monate
	public static void monate() {
		Scanner tastatur = new Scanner(System.in);  
		
		System.out.println("Geben Sie eine Zahl zwischen 1 und 12 ein:");
	    int monat = tastatur.nextInt();
	    
	    if(monat == 1) {
	    	System.out.println("Es ist Januar."); }
	    else if(monat == 2) {
	    	System.out.println("Es ist Februar."); }
	    else if(monat == 3) {
		   	System.out.println("Es ist M�rz"); }
		else if(monat == 4) {
		   	System.out.println("Es ist April."); }
	   	else if(monat == 5) {
	   		System.out.println("Es ist Mai."); }
	   	else if(monat == 6) {
	    	System.out.println("Es ist Juni."); }
	   	else if(monat == 7) {
	    	System.out.println("Es ist Juli."); }
	   	else if(monat == 8) {
	    	System.out.println("Es ist August."); }
	   	else if(monat == 9) {
	    	System.out.println("Es ist September."); }
	   	else if(monat == 10) {
	    	System.out.println("Es ist Oktober."); }
	   	else if(monat == 11) {
	    	System.out.println("Es ist November."); }
	   	else if(monat == 12) {
	    	System.out.println("Es ist Dezember."); }
	   	else {
	   		System.out.println("Sie m�ssen eine Zahl zwischen 1 und 12 eingeben."); }
	}
//Aufgabe 3 - R�mische Zahlen
	public static void roemischeZahlen() {
		Scanner tastatur = new Scanner(System.in);  
		
		System.out.println("Geben Sie eine r�mische Zahl ein:");
	    char rZahl = tastatur.next().charAt(0);
	
	    if(rZahl == 'I') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 1."); }
	    else if(rZahl == 'V') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 5."); }
	    else if(rZahl == 'X') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 10."); }
	    else if(rZahl == 'L') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 50."); }
	    else if(rZahl == 'C') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 100."); }
	    else if(rZahl == 'D') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 500."); }
	    else if(rZahl == 'M') {
	    	System.out.println("Die r�mische Zahl " + rZahl + " entspricht der arabischen Zahl 1000."); }
	    else {
	   		System.out.println("Sie m�ssen eine r�mische Zahl ein"); }
	}
	    
//Aufgabe 4 - Taschenrechner
	public static void taschenrechner() {
		Scanner tastatur = new Scanner(System.in);  
		
		System.out.println("Dies ist ein Taschenrechner. Geben Sie die erste Zahl ein:");
	    double zahl1 = tastatur.nextDouble();
	    
	    System.out.println("Geben Sie die zweite Zahl ein:");
	    double zahl2 = tastatur.nextDouble();
	    
	    System.out.println("Sie haben die Zahlen " + zahl1 + " und " + zahl2 + " gew�hlt. \nSie k�nnen die Zahlen addieren (1), subtrahieren (2), multiplizieren (3) und dividieren (4). Bitte w�hlen Sie aus diesen M�glichkeiten.");
	    int operation = tastatur.nextInt();
	    
	    if(operation==1) {
	    	double addieren = zahl1+zahl2;
	    	System.out.println("Das Ergebnis ist " + addieren); }
	    else if(operation==2) {
	    	double subtrahieren = zahl1-zahl2; 
	    	System.out.println("Das Ergebnis ist " + subtrahieren); }
	    else if(operation==3) {
	    	double multiplizieren = zahl1*zahl2;
		    System.out.println("Das Ergebnis ist " + multiplizieren); }
	    else if(operation==4) {
	    	double dividieren = zahl1/zahl2;
		    System.out.println("Das Ergebnis ist " + dividieren); }
	    else {
	    	System.out.println("W�hlen Sie eine Zahl zwischen 1 und 4 f�r die Rechenoperation."); }
	    	  
	    }
	    
	
	}
