
public class Konsoleausgabe {

	public static void main(String[] args) {
		
		double d1 = 22.4234234;
		System.out.printf( "%.2f\n" ,     d1);
		double d2 = 111.2222;
		System.out.printf( "%.2f\n" ,     d2);
		double d3 = 4.0;
		System.out.printf( "%.2f\n" ,     d3);
		double d4 = 1000000.551;
		System.out.printf( "%.2f\n" ,     d4);
		double d5 = 97.34;
		System.out.printf( "%.2f\n" ,     d5);
	
	
	

		
		String s0 = "0!" ;
		String s1 = "1!" ;
		String s2 = "2!";
		String s3 = "3!";
		String s4 = "4!";
		String ss = "5!";
		String s5 = "=";
		
		System.out.printf("%-5s", s0);
		System.out.printf("%s", s5);
		System.out.printf("%20s", s5);
		System.out.printf("%4s\n", 0);
		
		System.out.printf("%-5s", s1);
		System.out.printf("%s", s5);
		System.out.printf(" 1");
		System.out.printf("%18s", s5);
		System.out.printf("%4s\n", 1);
		
		System.out.printf("%-5s", s2);
		System.out.printf("%s", s5);
		System.out.printf(" 1 * 2");
		System.out.printf("%14s", s5);
		System.out.printf("%4s\n", 2);
		
		System.out.printf("%-5s", s3);
		System.out.printf("%s", s5);
		System.out.printf(" 1 * 2 * 3");
		System.out.printf("%10s", s5);
		System.out.printf("%4s\n", 6);
		
		System.out.printf("%-5s", s4);
		System.out.printf("%s", s5);
		System.out.printf(" 1 * 2 * 3 * 4");
		System.out.printf("%6s", s5);
		System.out.printf("%4s\n", 24);
		
		System.out.printf("%-5s", ss);
		System.out.printf("%s", s5);
		System.out.printf(" 1 * 2 * 3 * 4 * 5");
		System.out.printf("%2s", s5);
		System.out.printf("%4s\n", 120);
		// System.out.printf("-s1", s);
	}
}