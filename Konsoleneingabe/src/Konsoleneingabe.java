  
import java.util.Scanner; // Import der Klasse Scanner

public class Konsoleneingabe {

	public static void main(String[] args) {
		//Aufgabe1
		
		// TODO Auto-generated method stub
		// Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    double zahl1 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    double zahl2 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    double ergebnis1 = zahl1 + zahl2;  
	    double ergebnis2 = zahl1 - zahl2;
	    double ergebnis3 = zahl1 * zahl2;
	    double ergebnis4 = zahl1 / zahl2;
	    
	    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis1);   
	    System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
	    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis2);
	    System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
	    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis3);
	    System.out.print("\n\n\nErgebnis der Division lautet: ");
	    System.out.println(zahl1 + " : " + zahl2 + " = " + ergebnis4);
	    
	    
	
	    nameundalter();
		
	}
	
	public static void nameundalter() {
	  
		
		//Aufgabe2
		
		Scanner myScanner = new Scanner(System.in);  
		
		System.out.print("\n\nBitte geben Sie Ihren Namen ein: ");
		
		String name = myScanner.next();
		
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
	
		byte alter = myScanner.nextByte();
		
		System.out.print("Ihr Name lautet " + name + " und Sie sind " + alter + " Jahre alt. ");
	
		myScanner.close();
	}

}
