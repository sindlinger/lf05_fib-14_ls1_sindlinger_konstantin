package raumschiff;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ladung ladung = new Ladung();
	
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,"IKS Hegh'ta", 2, ladung);
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,"IRW Khazara", 2, ladung);
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,"NiVar", 5, ladung);

		Ladung L1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung L2 = new Ladung("Borg-Schrott", 5);
		Ladung L3 = new Ladung("Rote Materie", 2);
		Ladung L4 = new Ladung("Forschuungssonde", 35);
		Ladung L5 = new Ladung("Vat'leth Klingonen Schwert", 200);
		Ladung L6 = new Ladung("Plasma-Waffe", 50);
		Ladung L7 = new Ladung("Photononentorpedo", 3);

		klingonen.addLadung(L7);
		System.out.println(L2.getBezeichnung());
		System.out.println(L2.getAnzahl());
		System.out.println(klingonen.getLadungsverzeichnis());

		romulaner.addLadung(L2);
		System.out.println(L2.getBezeichnung());
		System.out.println(L2.getAnzahl());
		System.out.println(romulaner.getLadungsverzeichnis());
		
		vulkanier.addLadung(L4);
		System.out.println(L4.getBezeichnung());
		System.out.println(L4.getAnzahl());
		System.out.println(vulkanier.getLadungsverzeichnis());
		System.out.println(klingonen);
		System.out.println(romulaner);
		System.out.println(vulkanier);
		
		Raumschiff.nachrichtenVerzeichnis();
	}

	
}
