package raumschiff;

public class Ladung {
	
	private String bezeichnung;
	private double anzahl;
	
	public String toString() {
		return "\nLadung: \nBezeichnung=" + bezeichnung + ", \nAnzahl=" + anzahl;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(double anzahl) {
		this.anzahl = anzahl;
	
	}
	
	Ladung (String bezeichnung, double anzahl) {
		this.bezeichnung = bezeichnung;
		this.anzahl = anzahl;	
	}
	
	Ladung () {
		this.bezeichnung = "";
		this.anzahl = 0;
	}
	}

