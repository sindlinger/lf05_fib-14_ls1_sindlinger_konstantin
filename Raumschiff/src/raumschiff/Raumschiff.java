package raumschiff;
import java.util.ArrayList;


public class Raumschiff {

	private double photonentorpedo;
	private double energieversorgungInProzent;
	private double schildeInProzent;
	private double huelleInProzent;
	private double lebenserhaltungssystemeInProzent;
	private String schiffsname;
	private double reparaturandroiden;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();
	private static ArrayList<Broadcast> broadcast = new ArrayList<>();; //class ?
	
	
	//Getter & Setter	
	public double getPhotonentorpedo() {
		return photonentorpedo;
	}
	public void setPhotonentorpedo(double photonentorpedo) {
		this.photonentorpedo = photonentorpedo;
	}
	public double getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(double energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public double getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(double schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public double getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(double huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public double getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(double lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public double getReparaturandroiden() {
		return reparaturandroiden;
	}
	public void setReparaturandroiden(double reparaturandroiden) {
		this.reparaturandroiden = reparaturandroiden;
	}
	
	
	//Ladungsverzeichnis
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
		
	//Broadcast
	public static ArrayList<Broadcast> getBroadcast() {
		return broadcast;
	}
	public static void setBroadcast(ArrayList<Broadcast> broadcast) {
		Raumschiff.broadcast = broadcast;
	}
	
	//addLadung
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	
	Raumschiff (double photonentorpedo, double energieversorgung, double schilde, double huelle, double lebenserhaltungssysteme, String schiffsname, double reparaturandroiden, Ladung ladungsverzeichnis) {
		this.photonentorpedo = photonentorpedo;
		this.energieversorgungInProzent = energieversorgung;
		this.schildeInProzent = schilde;
		this.huelleInProzent = huelle;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssysteme;
		this.schiffsname = schiffsname;
		this.reparaturandroiden = reparaturandroiden;
		
	}
	
	public String toString() {
		return "\nRaumschiff \nPhotonentorpedo=" + photonentorpedo + ", \nEngergieversorgung in Prozent=" + energieversorgungInProzent + ",\nSchilde in Prozent=" + schildeInProzent + ", \nLebenserhaltungssysteme in Prozent=" + lebenserhaltungssystemeInProzent + ", \nHülle in Prozent=" + huelleInProzent + ", \nRaumschiffname=" + schiffsname + ", \nAndroidenzahl=" + reparaturandroiden + "\nLadungsverzeichnis=" + ladungsverzeichnis;	
	}
	//Zustand ausgeben
	public void zustand() {
		System.out.println("\nRaumschiff: \nPhotonentorpedo=" + photonentorpedo + ", \nEngergieversorgung in Prozent=" + energieversorgungInProzent + ",\nSchilde in Prozent=" + schildeInProzent + ", \nLebenserhaltungssysteme in Prozent=" + lebenserhaltungssystemeInProzent + ", \nHülle in Prozent=" + huelleInProzent + ", \nRaumschiffname=" + schiffsname + ", \nAndroidenzahl=" + reparaturandroiden + "\nLadungsverzeichnis=" + ladungsverzeichnis);	
	}
	
	//Photonentorpedo schießen
	public void photonenabschießen( ) {
		if(this.photonentorpedo == 0) {
			Broadcast nachricht = new Broadcast("\"-=*Click*=-\"");
			nachricht.schiffsname = getSchiffsname();
			addNachricht(nachricht); 
		}
		else {
			this.photonentorpedo -= 1;
		Broadcast nachricht = new Broadcast("Photonentorpedo abgeschossen!");
		addNachricht(nachricht);
		}
	}
	
	//Phaserkanone abschießen
	public void phaserkanonen() {
		if(this.energieversorgungInProzent < 50) {
			Broadcast nachricht = new Broadcast("\"-=*Click*=-\"");
			addNachricht(nachricht); 
		}
		else {
		this.energieversorgungInProzent -= 50;
		Broadcast nachricht = new Broadcast("Phaserkanone abgeschossen!");
			addNachricht(nachricht);
		}
	}

	//Nachrichtenmethode
	
	public static void nachrichtenVerzeichnis() {	
		
		for(Broadcast B :  broadcast ) {
		
			System.out.println(" Nachrichten from: "+B.schiffsname +" \n" );
			System.out.println(" Nachrichten: "+B.nachricht +" \n" );
			
		}
		
	}

	public void addNachricht(Broadcast Nachricht) {
		 broadcast.add(Nachricht);	
		} 
	
	public void sendeNachricht(String Nachricht ) {
		
		 System.out.println(" Raumschiff sendet eine Nachricht: "+Nachricht);
	}
	
	//Treffer anzeigen
	private void treffer(Raumschiff Raumschiff) {
		
		 System.out.printf(" Raumschiff %s wurde getroffen.",this.schiffsname );
	}
}		

