package Kiste;

public class Kiste {
	
	private double h�he;
	private double breite;
	private double tiefe;
	private String Farbe;
	
	Kiste (double h�he, double breite, double tiefe, String Farbe) {
		this.h�he = h�he;
		this.breite = breite;
		this.tiefe = tiefe;
		this.Farbe = Farbe;
	  	
	}
	public double getVolume() {
		return this.breite*this.h�he*this.tiefe;
	}
	
	public String Farbe() {
		return this.Farbe;
	}	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Kiste kosta1 = new Kiste(2,3,4, "Pink");
		Kiste kosta2 = new Kiste(3,4,5, "Blau");
		Kiste kosta3 = new Kiste(4,5,6, "Rot");
		System.out.println("Kosta1: \n" + "Volume: " + kosta1.getVolume()+ " Farbe: " + kosta1.Farbe());
		System.out.println("Kosta2: \n" + "Volume: " + kosta2.getVolume()+ " Farbe: " + kosta2.Farbe());
		System.out.println("Kosta3:\n" + "Volume: " + kosta3.getVolume()+ " Farbe: " + kosta3.Farbe());

	}

}